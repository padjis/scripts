#!/bin/sh

name='nexus_1'

docker volume create nexus_data

[[ $(docker ps -f "name=$name" --format '{{.Names}}') == $name ]] ||
 docker run -d -p 8081:8081 --name "$name" -v nexus_data:/nexus-data --restart always sonatype/nexus3
