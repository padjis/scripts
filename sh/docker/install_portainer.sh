#!/bin/sh

name='portainer_1'

docker volume create portainer_data

[[ $(docker ps -f "name=$name" --format '{{.Names}}') == $name ]] ||
  docker run -d --name "$name" -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock --restart always -v portainer_data:/data portainer/portainer
